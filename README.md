# OCTAVE

## Ejercicios básicos

### jmas1.m
Suma 1 a una variable inicializada en 0

### calcularj.m
Calcula la fórmula j=(j*2)+k

### calculark.m
Imprime el valor de k (while)

### area_circulo.m
Calcula el área de un círculo

### descuento15.m
calcula el descuento del 15 porciento del total de una compra

### est_aprob.m
Conoce si un estudiante aprobó, dada una calificación (if)

### nuevo_sueldo.m
Calcula el nuevo sueldo (if)

### num_may.m
Calcula el mayor de tres números ingresados (if)

### varias_respuestas.m
Obtiene el resultado de una función según el caso (switch)

### imp_5numeros.m
Calcula la suma de los primeros 5 números enteros (for)

### imp_1al100.m
Escribe los números del 1 al 100

### suma_num_usuario.m
Suma los números ingresados por el usuario (do-until)
